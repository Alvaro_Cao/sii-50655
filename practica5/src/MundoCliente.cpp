// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoCliente.h"
#include "glut.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <error.h>
#include <iostream>
#include <pthread.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

char* proyeccion;

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{

	//LOGGER (PASO 6):

	/*char cierre[100];
	char victoria[100];
	sprintf(victoria,"El jugador ha marcado 3 puntos.");
	sprintf(cierre, "---Cerrando juego...---");
	write(fifo1,victoria,strlen(victoria)+1);
	write(fifo1,cierre,strlen(cierre)+1);
	close(fifo1);
	munmap(proyeccion,sizeof(MemCompartida));*/

	// COORDENADAS CLIENTE (PASO 5):

	/*close(fifo_CS);
	unlink("tmp/fifoCS");*/

	// TECLADO CLIENTE (PASO 4):

	/*close(fifo_teclado);
	unlink("tmp/fifoteclado");

	PuntMemCompartida->accion=5;*/
	munmap(proyeccion,sizeof(MemCompartida));

}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;

		// LOGGER (PASO 5):

		/*char cad[100];
		sprintf(cad,"El jugador 2 ha marcado 1 punto, con un total de %d puntos.\n",puntos2);
		write(fifo1,cad,strlen(cad)+1);*/
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;

		// LOGGER (PASO 5):

		/*char cad[100];
		sprintf(cad,"El jugador 1 ha marcado 1 punto, con un total de %d puntos.\n",puntos1);
		write(fifo1,cad,strlen(cad)+1);*/
	}
	
	if(jugador1.Rebota(esfera))
	{
		jugador1.y1-=0.3;
		jugador2.y1+=0.3;
	}

	if(jugador2.Rebota(esfera))
	{
		jugador1.y1+=0.3;
		jugador2.y1-=0.3;
	}

	// TENIS (PASO 3):

	switch(PuntMemCompartida->accion) 
	{
		/*case 1:jugador1.velocidad.y=4;
			break;*/
		case 1: OnKeyboardDown('w',0,0);
			break;
		case 0:
			break; 
		/*case -1:jugador1.velocidad.y=-4;
			break;*/
		case -1: OnKeyboardDown('s',0,0);
			break;
	}
	
	PuntMemCompartida->esfera=esfera;
	PuntMemCompartida->raqueta1=jugador1;  

	// EJERCICIO PROPUESTO (LÍMITE DE PUNTOS):

	int Estado1=0;
	int Estado0=0;
	char cadena3[100];
	if(puntos1==3)
	{
		/*Estado1=1;
		if(Estado1!=Estado0)*/
		{
			/*sprintf(cadena3,"El jugador 1 ha marcado 3 puntos, el jugador 1 gana.");
			write(fifo1,cadena3,strlen(cadena3)+1);*/
			Estado0=Estado1;
			/*if(Estado0==1) */
				exit(0);
		}
	}
	if(puntos2==3)
	{
		/*Estado1=1;
		if(Estado1!=Estado0)
		{
			sprintf(cadena3,"El jugador 2 ha marcado 3 puntos, el jugador 2 gana.");
			write(fifo1,cadena3,strlen(cadena3)+1);
		}*/
		Estado0=Estado1;
		exit(0);
	}

	// COORDENADAS CLIENTE (PASOS 3 Y 4):

	char cadena_CS[200];
	//read(fifo_CS,cadena_CS,sizeof(cadena_CS));

	// SOCKET CLIENTE (PASO 5):

	socket_comun.Receive(cadena_CS,sizeof(cadena_CS));

	sscanf(cadena_CS,"%f %f %f %f %f %f %f %f %f %f %d %d", &esfera.centro.x,&esfera.centro.y,&jugador1.x1,&jugador1.y1,&jugador1.x2,&jugador1.y2,&jugador2.x1,&jugador2.y1,&jugador2.x2,&jugador2.y2,&puntos1,&puntos2);

}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;
	}

	// TECLADO CLIENTE (PASO 3):

	char teclado[10];
	sprintf(teclado,"%c",key);
	//write(fifo_teclado,teclado,sizeof(teclado)+1);

	// SOCKET CLIENTE (PASO 6):

	socket_comun.Send(teclado,sizeof(teclado));
}

void CMundo::Init()
{
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;

	// LOGGER (PASO 4):

	/*fifo1=open("/tmp/fifo1",O_WRONLY);*/

	// TENIS (PASO 4):

	int fd=open("/tmp/bot.txt", O_RDWR|O_CREAT|O_TRUNC, 0777);
	write(fd,&MemCompartida,sizeof(MemCompartida));
	proyeccion=(char*)mmap(NULL,sizeof(MemCompartida),PROT_WRITE|PROT_READ,MAP_SHARED,fd,0);
	close(fd);
	PuntMemCompartida=(DatosMemCompartida*)proyeccion;
	PuntMemCompartida->accion=0;

	// COORDENADAS CLIENTE (PASO 2):

	/*mkfifo("/tmp/fifoCS",0777);
	fifo_CS=open("/tmp/fifoCS", O_RDONLY);*/

	// TECLADO CLIENTE (PASO 2):

	/*mkfifo("/tmp/fifoteclado",0777);
	fifo_teclado=open("/tmp/fifoteclado", O_WRONLY);*/

	// SOCKET CLIENTE (PASO 2):
	
	char nombre_c[50];
	printf("Introduzca su nombre: \n");
	scanf("%s",nombre_c);

	// SOCKET CLIENTE (PASO 3):

	socket_comun.Connect((char*)"127.0.0.01",4800);

	// SOCKET CLIENTE (PASO 4):

	socket_comun.Send(nombre_c,sizeof(nombre_c));
}
