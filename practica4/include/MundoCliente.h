// Mundo.h: interface for the CMundo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_


#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <vector>
#include "Plano.h"
#include "Esfera.h"
#include "Raqueta.h"
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "DatosMemCompartida.h"
#include <sys/mman.h>
#include <pthread.h>

class CMundo  
{
public:
	void Init();
	CMundo();
	virtual ~CMundo();	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();	
	Esfera esfera;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;
	int puntos1;
	int puntos2;

	// LOGGER (PASO 3):

	int fifo1;

	// TENIS (PASOS 1 Y 2):

	DatosMemCompartida MemCompartida;
	DatosMemCompartida* PuntMemCompartida;

	// COORDENADAS CLIENTE (PASO 1):

	int fifo_CS;

	// TECLADO CLIENTE (PASO 1):	

	int fifo_teclado;

};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)

