#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include "DatosMemCompartida.h"
#include "Esfera.h"
#include "Raqueta.h"

int main()
{
	// BOT (PASO 1):

	int fd;
	DatosMemCompartida* PuntMemCompartida;
	char* proyeccion;

	// BOT (PASO 2):

	fd=open("/tmp/bot.txt",O_RDWR);
	proyeccion=(char*)mmap(NULL,sizeof(*(PuntMemCompartida)),PROT_WRITE|PROT_READ,MAP_SHARED,fd,0);

	// BOT (PASO 3):

	close(fd);

	// BOT (PASO 4):

	PuntMemCompartida=(DatosMemCompartida*)proyeccion;

	// BOT (PASO 5):

	while(1)
	{
		float PosRaqueta;
		PosRaqueta=((PuntMemCompartida->raqueta1.y2+PuntMemCompartida->raqueta1.y1)/2);		//Designación del punto central de la raqueta.
		if(PosRaqueta < PuntMemCompartida->esfera.centro.y)
			PuntMemCompartida->accion=1;													//Si el centro de la raqueta se encuentra verticalmente por debajo del centro de la esfera, la raqueta subirá.
		else if(PosRaqueta > PuntMemCompartida->esfera.centro.y)
			PuntMemCompartida->accion=-1;													//Si el centro de la raqueta se encuentra verticalmente por encima del centro de la esfera, la raqueta bajará.
		else
			PuntMemCompartida->accion=0;													//Si el centro de la raqueta se encuentra verticalmente alineado con el centro de la esfera, la raqueta no se moverá.
		usleep(25000);
	}
	munmap(proyeccion,sizeof(*(PuntMemCompartida)));
}