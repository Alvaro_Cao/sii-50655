#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

int main(int argc, char* argv[])
{

	// LOGGER (PASOS 1 Y 2):

	mkfifo("/tmp/fifo1", 0777);
	int fd=open("/tmp/fifo1", O_RDONLY);
	int salida=0;
	int aux;
	while(salida==0) 
	{
		char buffer[100];
		aux=read(fd,buffer,sizeof(buffer));
		printf("%s\n",buffer);
		if(buffer[0]=='-')
		{
			printf("---Juego cerrado, cerrando logger...---\n");
			salida=1; 
		}
	}
	close(fd);
	unlink("/tmp/fifo1");
	return 0;
};